package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import main.Telivision;
import main.TelivisionServiceArrayImpl;

public class TestArrayList {
	
	TelivisionServiceArrayImpl telivisionServiceArrayImpl = new TelivisionServiceArrayImpl(3);
	
	//@Test	
public void add() {
	telivisionServiceArrayImpl.addTelivision(new Telivision("lg",21000,LocalDate.parse("2021-01-01")));
	assertEquals(1,telivisionServiceArrayImpl.getCount());
	
}
	
	
	
	//@Test
  public void test() {
		
		telivisionServiceArrayImpl.addTelivision(new Telivision("lg",21000,LocalDate.parse("2021-01-01")));
		assertEquals(1,telivisionServiceArrayImpl.getCount());
		telivisionServiceArrayImpl.addTelivision(new Telivision("lg",21000,LocalDate.parse("2021-01-01")));		
		assertEquals(2,telivisionServiceArrayImpl.getCount());
	}
@Test
	public void search() {
		
	telivisionServiceArrayImpl.addTelivision(new Telivision("lg",21000,LocalDate.parse("2021-01-01")));
	telivisionServiceArrayImpl.addTelivision(new Telivision("sony",21000,LocalDate.parse("2021-01-01")));
	telivisionServiceArrayImpl.addTelivision(new Telivision("mi",21000,LocalDate.parse("2021-01-01")));
	telivisionServiceArrayImpl.addTelivision(new Telivision("samsung",21000,LocalDate.parse("2021-01-01")));
	assertEquals(4,telivisionServiceArrayImpl.getCount());
	
	}

	
}
