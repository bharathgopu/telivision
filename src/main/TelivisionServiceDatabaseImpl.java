package main;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.postgresql.jdbc.PgResultSet;

public class TelivisionServiceDatabaseImpl implements TelivisionService {

	String username;
	String password;
	String driver;
	String url;

	
	public TelivisionServiceDatabaseImpl(String username, String password, String driver, String url) {
		this.username = username;
		this.password = password;
		this.driver = driver;
		this.url = url;
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		return DriverManager
				.getConnection(url,username,password);
				
	}
	private Telivision getTelivision(ResultSet resultSet) {
		 
		Telivision telivision = new Telivision();
		
			  try {
				telivision.setBrandName(resultSet.getString("brand_name"));
				 telivision.setPrice(resultSet.getInt("price"));
				  telivision.setDeliveryDate(resultSet.getDate("delivery_date").toLocalDate());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return telivision;
		
	}
	@Override
	public void addTelivision(Telivision telivision) {
		try {
		Connection connection = getConnection();
		String query = "INSERT INTO telivision(brand_name,price,delivery_date) VALUES(?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		preparedStatement.setString(1,telivision.getBrandName());
		preparedStatement.setInt(2,telivision.getPrice());
		preparedStatement.setDate(3,java.sql.Date.valueOf(telivision.getDeliveryDate()) );
		preparedStatement.executeUpdate();
		connection.close();
		 } catch (Exception e) {
	         e.printStackTrace();
	         System.err.println(e.getClass().getName()+": "+e.getMessage());
	         System.exit(0);
	      }
	      System.out.println("Opened database successfully");
	   }
	
	
		
	

	@Override
	public void displayAll() {
	 Connection connection;
	 try {
		connection = getConnection();
		String query = "SELECT * FROM telivision";
		PreparedStatement preparedStatement =  connection.prepareStatement(query);
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			
			Telivision telivision = getTelivision(resultSet);
			System.out.println(telivision);
//			String brand_name = result.getString("brand_name");
//			int price = result.getInt("price");
//			Date delivery_date = result.getDate("delivery_date");
//			
//			
//			
//			System.out.println("Brand Name: " +brand_name );
//			System.out.println("Price"+ price);
//			System.out.println("Delivery Date: "+ delivery_date+ "\n");
//			
//			
//			
		}
		
		connection.close();
	} catch (ClassNotFoundException | SQLException e) {
		
		e.printStackTrace();
	}
		 
}

	@Override
	public void search(String brandName) {
		try {
			Connection connection ;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM telivision where brand_name=?");
			preparedStatement.setString(1, brandName);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())

			{
				Telivision telivision = getTelivision(resultSet);
				System.out.println(telivision);	
				
//			String brand_name = resultSet.getString("brand_name");
//			int price = resultSet.getInt("price");
//			Date date = resultSet.getDate("delivery_date");
//			
//
//
//			System.out.println("Brandname :" + brand_name);
//			System.out.println("price :" + price);
//			System.out.println("order date :" + date);
			}
			connection.close();



			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}




			}
		
		

	@Override
	public void sort() {
		Connection connection ;
		try {
			connection = getConnection();		
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM telivision ORDER by price ");
			
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Telivision telivision = getTelivision(resultSet);
				System.out.println(telivision);			
				
//				String brand_name = resultSet.getString("brand_name");
//				int price = resultSet.getInt("price");
//				Date date = resultSet.getDate("delivery_date");
//				
//
//
//				System.out.println("Brandname :" + brand_name);
//				System.out.println("price :" + price);
//				System.out.println("order date :" + date);
				}
				connection.close();
				
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

	@Override
	public void delete(String brand_name) {
		Connection connection ;
		try {
			connection = getConnection();		
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM telivision WHERE brand_name = ?");
			preparedStatement.setString(1, brand_name);
			preparedStatement.executeUpdate();
			System.out.println("Successfully Deleted");			
				connection.close();
				
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		}

	

	@Override
	public  List<Telivision> search(LocalDate deliveryDate) {
		List<Telivision>telivisionlist = new ArrayList<Telivision>();

		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement prepareStatement = connection.prepareStatement("SELECT * FROM telivision WHERE delivery_date=?");
			prepareStatement.setDate(1,Date.valueOf(deliveryDate));
			ResultSet resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				Telivision telivision = getTelivision(resultSet);
				System.out.println(telivision);	
				
//			String name1 = resultset.getString("brand_name");
//			int price = resultset.getInt("price");
//			Date delivery_date = resultset.getDate("delivery_date");
//			System.out.println("name" + name1);
//			System.out.println("price" + price);
//			System.out.println("manufatured_date" + delivery_date);
			}
			connection.close();
			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}
		return telivisionlist ;
		
	}


	@Override
	public void downloadDetailsAsCSV() throws IOException {
		Connection connection ;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from telivision");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("telivision database details.csv");
			while (resultSet.next()) {

			String data =getCSVString(getTelivision(resultSet));
			file.write(data);
			}
			file.close();
			} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
			
		}

	

	@Override
	public void downloadDetailsAsJson() throws IOException {
		Connection connection ;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from telivision");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("telivision_database_details.txt");
		 	StringJoiner join = new StringJoiner(",","[","]");
			//file.append("[");
			while (resultSet.next()) {
				String data = join.add(getJsonString(getTelivision(resultSet))).toString();
				//String data =getJsonString(getTelivision(resultSet));
              	file.write(data);
//				 if(!resultSet.isBeforeFirst()&& !resultSet.isLast()) {
//			           file.append(",");		
//						}
			}
			//file.append("]");
			file.close();
			connection.close();
		
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}

	}

}
