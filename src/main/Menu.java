package main;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Menu {

	public static void main(String[] args) throws FileNotFoundException, IOException{
             
		Scanner sc = new Scanner(System.in);
		FileReader reader =new FileReader("src/config.properties");
		Properties properties = new Properties();
		properties.load(reader);
		
		//TelivisionService tvServices = new TelivisionServiceArrayImpl();
		TelivisionService tvServices = TelivisionServiceFactory.getTelivisionservice(properties);
		int number;
 
		do {
			System.out.println("MENU");
			System.out.println("1.Add Television ");
			System.out.println("2.Display all details");
			System.out.println("3.Search television  details");
			System.out.println("4.sort  tv details by price");
			System.out.println("5.search  tv details by delivery date");
			System.out.println("6.Delete the Telivision based on name");
			System.out.println("7.Download the details as CSV file:");
			System.out.println("8.Download the details as Json file:");
			System.out.println("Enter 0 to exit");
			System.out.println("Please Enter any of the above option : ");
			number = sc.nextInt();
			switch (number) {
			case 1:

				System.out.println("enter brand name:");
				String a1 = sc.next();
				System.out.println("enter the price :");
				int b1 = sc.nextInt();
				System.out.println("Enter the date");
				LocalDate deliverDate = LocalDate.parse(sc.next());
				Telivision tv1 = new Telivision(a1, b1, deliverDate);
				try {
					tvServices.addTelivision(tv1);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}catch(NullPointerException Exception  ) {
					Exception .printStackTrace();
					
				}
					
				
				
				
				break;
			case 2:
				tvServices.displayAll();
				break;
			case 3:
				System.out.println("Enter the Name :");
				String name1 = sc.next();
				tvServices.search(name1);
				break;
			case 4:
				tvServices.sort();
				break;
			case 5:
				System.out.println("Enter the date  :");
				String str = sc.next();
				LocalDate date = LocalDate.parse(str);
			List<Telivision> tvList = tvServices.search(date);
				System.out.println(tvList);
				break;
			case 6:
				System.out.println("Enter the Brand Name: ");
				String Str = sc.next();
				tvServices.delete(Str);
			case 7:
				try {
					tvServices.downloadDetailsAsCSV();
					} catch (Exception e) {

							e.printStackTrace();
					}

				break;
			case 8:
				try {
					tvServices.downloadDetailsAsJson();
					} catch (Exception e) {

							e.printStackTrace();
					 }
				break;
			default:
				System.out.println("invalid number");

				break;
			}

		} while (number != 0);

	}

}