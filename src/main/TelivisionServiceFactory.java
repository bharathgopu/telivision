package main;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public abstract class TelivisionServiceFactory {
	public static TelivisionService getTelivisionservice(Properties properties) throws IOException {
		FileReader reader =new FileReader("src/config.properties");
		Properties prop = new Properties();
		prop.load(reader);
	String username = prop.getProperty("user");
		String password =prop.getProperty("password");
		 String driver =prop.getProperty("driver");
		 String url =prop.getProperty("url");
		String type = prop.getProperty("implementation");
		type =type.toUpperCase();
		
		TelivisionService service = null;
		
		switch(type) {
		case "ARRAY":
			service = new TelivisionServiceArrayImpl();
			break;
		case "ARRAYLIST":
			service = new TelivisionServiceArrayListImpl();
			break;
		
		case"DATABASE":
			service = new TelivisionServiceDatabaseImpl(username, password, driver, url);
			break;
			default:
				System.out.println("Invalid");
		}	
		return service;
	}

}
