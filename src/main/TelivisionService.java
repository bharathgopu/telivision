package main;


import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;



// a interface specifies the operations on Television object
//  @Mi
// Samsung

public interface TelivisionService {
     //    This method will add Telivison object and its details
    //	  @Samsung is to added
   //
	void addTelivision(Telivision telivision) throws ClassNotFoundException, SQLException;//
     //This method we enter the deatils of telivisions through input method
     //price, deliver date
     //	
	void displayAll();//
	// This method display the all the details os Telivison 
	//

	void search(String brandName);
	//  this method shows the details of teilivision ,when we input the brand name
	//

	void sort();
	// This method sorts the telivision price and diplays them.
	//
	void delete(String name);
	// This method deletes all telivisions based on brandName

	List<Telivision> search(LocalDate deliveryDate);
  // This method will search the telivision and display the telivision with delivery date 
	
	
	
		
	default String getCSVString(Telivision telivision) {
		StringBuffer str = new StringBuffer();
		str.append(telivision.getBrandName());
		str.append("\",");
		str.append(telivision.getPrice());
		str.append("\",");
		str.append(telivision.getDeliveryDate());
		return str.toString();
		
		
		
	}
	default String getJsonString(Telivision telivision){
		return new StringBuffer().append("{").append("\n").append("\"brandName\":").append("\"").append(telivision.getBrandName())
				.append("\"").append(",").append("\n").append("\"price\":").append(telivision.getPrice())
				.append(",").append("\n").append("\"deliveryDate\":").append("\"")
				.append(telivision.getDeliveryDate()).append("\"").append("}").toString();
		
		
	}
	
	void downloadDetailsAsCSV() throws IOException;
	// This method downloads the all details of Television in CSV format
	
	void downloadDetailsAsJson()throws IOException;
  // This methos downloads th all details of television	in Json format
	

}

