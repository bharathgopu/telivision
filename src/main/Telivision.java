package main;


import java.time.LocalDate;
import java.time.LocalDateTime;

public class Telivision implements Comparable<Telivision>{
	private String brandName;  
    private	int price;
    LocalDate deliveryDate;  
    
     public Telivision(String brandName,int price,LocalDate deliveryDate) {
    	this.brandName= brandName;
    	this.price = price;
    	this.deliveryDate = deliveryDate;
    	 
     }
	
	public Telivision() {
		
	}


	public String getBrandName() {
		return brandName;
	}


	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Telivision other = (Telivision) obj;
		if (brandName == null) {
			if (other.brandName != null)
				return false;
		} else if (!brandName.equals(other.brandName))
			return false;
		if (deliveryDate == null) {
			if (other.deliveryDate != null)
				return false;
		} else if (!deliveryDate.equals(other.deliveryDate))
			return false;
		if (price != other.price)
			return false;
		return true;
	}


@Override
	public String toString() {
	StringBuffer str = new StringBuffer();
	str.append("brand name = " + brandName);
	str.append("\nprice ="+ price);
	str.append("\ndeivery date =" + deliveryDate);
		
		 return  str.toString();
	}



/*
 * public void details(Scanner sc) {
 * 
 * System.out.println("Enter the Brand name: "); this.brand_name= sc.next();
 * System.out.println("Enter the  price : "); this.price= sc.nextInt(); }
 */

@Override
public int compareTo(Telivision tvservices) {
//	if(this.price < (tvservices.getPrice()))
//	return -1;
//	else if(this.price > (tvservices.getPrice()))
//		return 1;
//	else
//		return 0;
	return price-tvservices.price;
			
}

	
}





