

import java.time.LocalDate;
import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		TelivisionService tvServices = new TelivisionServiceImpl();
		int number;

		do {
			System.out.println("MENU");
			System.out.println("1.Add Television ");
			System.out.println("2.Display all details");
			System.out.println("3.Search television  details");
			System.out.println("4.sort  tv details by price");
			System.out.println("5.search  tv details by delivery date");
			System.out.println("6.Download the details as CSV file:");
			System.out.println("7.Download the details as Json file:");
			System.out.println("Enter 0 to exit");
			System.out.println("Please Enter any of the above option : ");
			number = sc.nextInt();
			switch (number) {
			case 1:

				System.out.println("enter brand name:");
				String a1 = sc.next();
				System.out.println("enter the price :");
				int b1 = sc.nextInt();
				System.out.println("Enter the date");
				LocalDate deliverDate = LocalDate.parse(sc.next());
				Telivision tv1 = new Telivision(a1, b1, deliverDate);
				tvServices.addTelivision(tv1);
				break;
			case 2:
				tvServices.displayAll();
				break;
			case 3:
				System.out.println("Enter the Name :");
				String name1 = sc.next();
				tvServices.search(name1);
				break;
			case 4:
				tvServices.sort();
				break;
			case 5:
				System.out.println("Enter the date  :");
				String str = sc.next();
				LocalDate date = LocalDate.parse(str);
				tvServices.search(date);
				break;
			case 6:
				try {
				tvServices.downloadDetailsAsCSV();
				} catch (Exception e) {

						e.printStackTrace();
				}

				break;
			case 7:
				try {
				tvServices.downloadDetailsAsJson();
				} catch (Exception e) {

						e.printStackTrace();
				 }
				break;
			default:
				System.out.println("invalid number");

				break;
			}

		} while (number != 0);

	}

}